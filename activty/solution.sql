--a.
SELECT *
	FROM artists
	WHERE name LIKE '%a%';


--b
SELECT *
	FROM songs
	WHERE length < '3:30';



--c
SELECT album_title, song_name, length
	FROM albums JOIN songs
	ON albums.id = songs.album_id;


--d
SELECT *
	FROM artists JOIN albums
	ON artists.id = albums.artist_id
	WHERE album_title LIKE '%a%';



--e
SELECT *
	FROM albums
	ORDER BY album_title desc
	LIMIT 4;



--f
SELECT *
	FROM albums JOIN songs
	ON albums.id = songs.album_id
	ORDER BY album_title desc;